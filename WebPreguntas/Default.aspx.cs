﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPreguntas
{
    public partial class _Default : Page
    {
        private int respuestas_buenas = 0;

        private string pregunta_1 = "c";
        private string pregunta_2 = "b";
        private string pregunta_3 = "d";
        private string pregunta_4 = "b";
        private string pregunta_5 = "a";
        private string pregunta_6 = "b";
        private string pregunta_7 = "d";
        private string pregunta_8 = "c";
        private string pregunta_9 = "c";
        private string pregunta_10 = "a";
    

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["flash"] = respuestas_buenas; 

            if (!IsPostBack)
            {
                
            }    

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            if (RadioButtonList1.SelectedValue.Equals(pregunta_1))
            {
                respuestas_buenas += 1;

                Label11.Text = "La respuesta es correcta";

            }
            else
            {
                Label11.Text = "La respuesta correcta es la : " + pregunta_1;
            }
            if (RadioButtonList2.SelectedValue.Equals(pregunta_2))
            {
                respuestas_buenas += 1;
                Label12.Text = "La respuesta es correcta";
            }
            else
            {
                Label12.Text = "La respuesta correcta es la : " + pregunta_2;
            }

            if (RadioButtonList3.SelectedValue.Equals(pregunta_3))
            {
                respuestas_buenas += 1;
                Label13.Text = "La respuesta es correcta";
            }
            else
            {
                Label13.Text = "La respuesta correcta es la : " + pregunta_3;
            }
            if (RadioButtonList4.SelectedValue.Equals(pregunta_4))
            {
                respuestas_buenas += 1;
                Label14.Text = "La respuesta es correcta";
            }
            else
            {
                Label14.Text = "La respuesta correcta es la : " + pregunta_4;
            }
            if (RadioButtonList5.SelectedValue.Equals(pregunta_5))
            {
                respuestas_buenas += 1;
                Label15.Text = "La respuesta es correcta";
            }
            else
            {
                Label15.Text = "La respuesta correcta es la : " + pregunta_5;
            }

            if (RadioButtonList6.SelectedValue.Equals(pregunta_6))
            {
                Label16.Text = "La respuesta es correcta";
                respuestas_buenas += 1;
            }
            else
            {
                Label16.Text = "La respuesta correcta es la : " + pregunta_6;
            }
            if (RadioButtonList7.SelectedValue.Equals(pregunta_7))
            {
                Label17.Text = "La respuesta es correcta";
                respuestas_buenas += 1;
            }
            else
            {
                Label17.Text = "La respuesta correcta es la : " + pregunta_7;
            }
            if (RadioButtonList8.SelectedValue.Equals(pregunta_8))
            {
                Label18.Text = "La respuesta es correcta";
                respuestas_buenas += 1;
            }
            else
            {
                Label18.Text = "La respuesta correcta es la : " + pregunta_8;
            }

            if (RadioButtonList9.SelectedValue.Equals(pregunta_9))
            {
                Label19.Text = "La respuesta es correcta";
                respuestas_buenas += 1;
            }
            else
            {
                Label19.Text = "La respuesta correcta es la : " + pregunta_9;
            }

            if (RadioButtonList10.SelectedValue.Equals(pregunta_10))
            {
                Label20.Text = "La respuesta es correcta";
                respuestas_buenas += 1;
            }
            else
            {
                Label20.Text = "La respuesta correcta es la : " + pregunta_10;
            }
            Session["flash"]=respuestas_buenas;
            Rta.Text = respuestas_buenas.ToString();

        }
    }
}