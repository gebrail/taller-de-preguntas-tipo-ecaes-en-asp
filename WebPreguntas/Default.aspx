﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebPreguntas._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h2>PREGUNTAS TIPO ECAES, ING DE SOFTWARE</h2>        
    </div>
    <%
if ((int)Session["flash"]>0) 
{%>

           <div class="alert alert-success" role="alert">
             <strong>El resultado de esta prueba es que has tenido  <asp:Label ID="Rta" runat="server" Text="Label"></asp:Label> respuestas bien de 10</strong><br />
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
<% } 
 %>


 <%
if ((int)Session["flash"]==0) 
{%>

           <div class="alert alert-danger" role="alert">
             <strong>Ninguna respuesta correcta lo siento</strong><br />
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
<% } 
 %>

    <div class="row">
        <div class="col-md-12">




            <br />

            <asp:Label ID="Label1" runat="server"></asp:Label> <br />

            <asp:Label ID="Label11" runat="server"></asp:Label><br>

            1- Para la construcción del sistema de información, TENSOFT propone trabajar un modelo de ciclo de vida de software que permita evaluar cada una de sus fases y cambios de requerimientos. De acuerdo con lo anterior, la metodología de desarrollo por utilizar y su respectiva secuencia de actividades son:
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" required="true">

                 <asp:ListItem Value="a">a.Cascada; análisis de requerimientos, diseño, codificación, pruebas, mantenimiento.</asp:ListItem>
                <asp:ListItem Value="b">b.Prototipos; plan rápido, modelo diseño rápido, construcción del prototipo, comunicación,</asp:ListItem>
                <asp:ListItem Value="c">c.Espiral; comunicación con el cliente, planificación, análisis de riesgos, ingeniería, evaluación</asp:ListItem>
                <asp:ListItem Value="d">d.Incremental; combinación de elementos repetitivamente, secuencias lineales, construcción</asp:ListItem>
            </asp:RadioButtonList>
            <br />
           <asp:Label ID="Label12" runat="server"></asp:Label>            <br>
            2- Como una parte de la estrategia de aseguramiento de calidad durante el proceso de construcción de sistemas de información, se debe elaborar y aplicar un plan de pruebas que permita verificar detalles procedimentales de estos. El método de pruebas más adecuado es el de :<br />
             <asp:RadioButtonList ID="RadioButtonList2" runat="server">
                <asp:ListItem Value="a">a.caja negra.</asp:ListItem>
                <asp:ListItem Value="b">b.caja blanca. </asp:ListItem>
                <asp:ListItem Value="c">c.flujo de datos.</asp:ListItem>
                <asp:ListItem Value="d">d.flujo de control.</asp:ListItem>
            </asp:RadioButtonList>
            <br />
                       <asp:Label ID="Label13" runat="server"></asp:Label><br>
            3. TENSOFT deberá diseñar la arquitectura del sistema de información de acuerdo con los requerimientos planteado por TPMENS. En este caso, la arquitectura más adecuada es la de<br />
            <br />
            <asp:RadioButtonList ID="RadioButtonList3" runat="server">
                <asp:ListItem Value="a">a.repositorio</asp:ListItem>
                <asp:ListItem Value="b">b.eventos.</asp:ListItem>
                <asp:ListItem Value="c">c.aspectos</asp:ListItem>
                <asp:ListItem Value="d">d.multiniveles.</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <br />           <asp:Label ID="Label14" runat="server"></asp:Label><br>
                        4- Para posibilitar la integración de los componentes del nuevo sistema de la empresa, junto a los aplicativos existentes, de manera que no afecte el traspaso de información entre los clientes, seutilizan mecanismos de coordinación para la transferencia de dicha información.La arquitectura que debe utilizarse es
            <asp:RadioButtonList ID="RadioButtonList4" runat="server">
                <asp:ListItem Value="a">a. por capas</asp:ListItem>
                <asp:ListItem Value="b">b.centrada en datos.</asp:ListItem>
                <asp:ListItem Value="c">c.orientada por eventos.</asp:ListItem>
                <asp:ListItem Value="d">d.orientada por objetos</asp:ListItem>
            </asp:RadioButtonList>
            <br />           <asp:Label ID="Label15" runat="server"></asp:Label><br>
            5- . El desarrollo del sistema de información implica que la empresa Tensoft realice una serie de etapas establecidas como el ciclo de vida, una de ellas es el estudio de factibilidad que identifica las necesidades por satisfacer con la aplicación computacional. Teniendo en cuenta lo anterior, en el desarrollo de este sistema, el aspecto principal para el desarrollo del proyecto es <br />
            <asp:RadioButtonList ID="RadioButtonList5" runat="server">            
                <asp:ListItem Value="a">a.técnico..</asp:ListItem>
                <asp:ListItem Value="b">b.económico. </asp:ListItem>
                <asp:ListItem Value="c">c.operacional..</asp:ListItem>
                <asp:ListItem Value="d">d.administrativo.</asp:ListItem>
            </asp:RadioButtonList>
            <br />           <asp:Label ID="Label16" runat="server"></asp:Label><br>
            6- En el contexto de la programación orientada a objetos es posible realizar operaciones de una clase sin instanciar objetos de la misma, porque<br />
            <br />
            <asp:RadioButtonList ID="RadioButtonList6" runat="server">
                <asp:ListItem Value="a">a. se pueden invocar métodos heredados de su superclase.</asp:ListItem>
                <asp:ListItem Value="b">b. se pueden invocar métodos estáticos de la misma clase.</asp:ListItem>
                <asp:ListItem Value="c">c. los métodos de la clase pueden haber sido redefinidos.</asp:ListItem>
                <asp:ListItem Value="d">d. los métodos de la clase pueden haber sido sobrecargados.</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <br />
           <asp:Label ID="Label17" runat="server"></asp:Label><br>

            7- . El polimorfismo es una propiedad potente dentro del paradigma de orientación a objetos, el cual se implementa a menudo mediante la técnica de redefinición de métodos. Un método puede ser redefinido en una subclase si es marcado en la superclase como
            <asp:RadioButtonList ID="RadioButtonList7" runat="server">
                <asp:ListItem Value="a">a. abreviado.</asp:ListItem>
                <asp:ListItem Value="b">b. sobrecargado.</asp:ListItem>
                <asp:ListItem Value="c">c. estático.</asp:ListItem>
                <asp:ListItem Value="d">d. abstracto.</asp:ListItem>
            </asp:RadioButtonList>
            <br />           <asp:Label ID="Label18" runat="server"></asp:Label><br>
            8- . Para recorrer una colección sin depender de su implementación, se puede utilizar un patrón de diseño de tipo <br />
            <asp:RadioButtonList ID="RadioButtonList8" runat="server">            
                <asp:ListItem Value="a">a. prototipo, porque conviene realizar ensayos de recorridos antes de decidir una
                implementación definitiva.</asp:ListItem>
                <asp:ListItem Value="b">b. interfaz, porque de esta manera se establece un comportamiento abstracto que puede
                implementarse posteriormente.</asp:ListItem>
                <asp:ListItem Value="c">c. iterador, porque este define operaciones de avance, retroceso y detección de terminación
                en una colección abstracta.</asp:ListItem>
                <asp:ListItem Value="d">d. fachada, porque este abstrae la selección de los servicios de un sistema sin importar su
                implementación.</asp:ListItem>
            </asp:RadioButtonList>
            <br />           <asp:Label ID="Label19" runat="server"></asp:Label><br>
            9- En la arquitectura modelo-vista-controlador (MVC) los objetos del mundo del problema se representan mediante clases de tipo <br />
            <br />
            <asp:RadioButtonList ID="RadioButtonList9" runat="server">
                <asp:ListItem Value="a">a. interfaz.</asp:ListItem>
                <asp:ListItem Value="b">b. frontera.</asp:ListItem>
                <asp:ListItem Value="c">c. entidad.</asp:ListItem>
                <asp:ListItem Value="d">d. control.</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <br />            

           <asp:Label ID="Label20" runat="server"></asp:Label><br>
            10- . Una empresa de desarrollo de software centra su negocio en la construcción de soluciones aplicando el desarrollo basado en componentes, lo cual le ha permitido reutilizar componentes de acuerdo con las características de funcionalidad, diseño e implementación de cada proyecto. En el proceso de aseguramiento de calidad de un producto, la gerencia del proyecto determinó disminuir el tiempo y los recursos asignados al inicio del proyecto y a la ejecución de pruebas de unidad, dado que
            <asp:RadioButtonList ID="RadioButtonList10" runat="server">
                <asp:ListItem Value="a">a. al reutilizar componentes se espera que estos hayan sido probados suficientemente antes
                de entregarlos</asp:ListItem>
                <asp:ListItem Value="b">b. en el enfoque de componentes el aseguramiento de calidad con pruebas de validación no
                es importante</asp:ListItem>
                <asp:ListItem Value="c">c. las pruebas de aceptación son más importantes que las demás</asp:ListItem>
                <asp:ListItem Value="d">d. la prueba sobre la interoperabilidad de componentes garantiza el funcionamiento de cada
                componente</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <br />

            <asp:Button class="btn btn-block btn-primary" ID="Button1" runat="server" Text="Enviar respuestas" OnClick="Button1_Click" />

            <br />
            <br />
            <br />            
        </div>
    </div>

</asp:Content>
